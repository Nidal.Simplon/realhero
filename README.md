# Project Title

Real Hero is a classic shoot them up.

## Getting Started

Download the files in the repository or clone it.

### Installing

Clone this repository :

```
git@gitlab.com:Nidal.Simplon/realhero.git
```

## Deployment

Start the game by opening the index file in Firefox.

## Built With

JavaScript, HTML and CSS.

## Author

* **Nidal Zaiani** - [RealHero](https://gitlab.com/Nidal.Simplon/realhero)

## Special Thanks

* Jean Demel (lead developper)
* Pierre Brogard
* Simplon.co

