


/*variable reutilisable n'importe ou dans le code*/

let divSquare = document.querySelector(".vaisseau");
let body = document.querySelector('body');
let playground = document.querySelector(".midground")

/*Lecture de la musique du jeu*/

playMusic();
console.log(playMusic());




let squareData = {
    x: 0,
    y: 0,
    w: 20,
    h: 20,
};

/*Variable des movement diagonales de divSquare pour les deux event*/

let right = false;
let left = false;
let up = false;
let down = false;
let shoot = false;

/*Event pour les mouvement de divSquare*/

body.addEventListener('keydown', function (event) {

    if (event.code === 'ArrowRight' && divSquare.offsetLeft <= 1630) {
        right = true;

        divSquare.style.display = 'block';
        squareData.x = squareData.x + 20;
    }
    if (event.code === 'ArrowLeft' && divSquare.offsetLeft >= 10) {
        left = true;

        divSquare.style.display = 'block';
        squareData.x = squareData.x - 20;
    }
    if (event.code === 'ArrowUp' && divSquare.offsetTop >= 10) {
        up = true;

        divSquare.style.display = 'block';
        squareData.y = squareData.y - 20;
    }


    if (event.code === 'ArrowDown' && divSquare.offsetTop <= 660) {
        down = true;

        divSquare.style.display = 'block';
        squareData.y = squareData.y + 20;


    }
    // if (event.code === 'KeyQ') {
    //     hadoken.style.display = 'block';
    //     divSquare.style.display = 'none';
    //     console.log(event.code);
    //     drawSquare(squareData);

    // }
    // if (event.code === 'KeyR') {
    //     shoot = true;

    // }
    console.log(event);

});

/*Deuxieme Event pour les mouvement de divSquare*/



body.addEventListener('keyup', function (event) {

    if (event.code === 'ArrowRight' /*&& divSquare.offsetLeft <= 1630*/) {
        right = false;

        divSquare.style.display = 'block';
        squareData.x = squareData.x + 20;
    }
    if (event.code === 'ArrowLeft' /*&& divSquare.offsetLeft >= 10*/) {
        left = false;

        divSquare.style.display = 'block';
        squareData.x = squareData.x - 20;
    }
    if (event.code === 'ArrowUp' /*&& divSquare.offsetTop >= 10*/) {
        up = false;

        divSquare.style.display = 'block';
        squareData.y = squareData.y - 20;
    }

    if (event.code === 'ArrowDown' /*&& divSquare.offsetTop <= 660*/) {
        down = false;

        divSquare.style.display = 'block';
        squareData.y = squareData.y + 20;


    }
    // if (event.code === 'KeyQ') {
    //     hadoken.style.display = 'block';
    //     divSquare.style.display = 'none';
    //     console.log(event.code);
    //     drawSquare(squareData);

    // }
    if (event.code === 'KeyR') {
        shoot = true;
        bulletShootPlayer();

    }



});


/*Interval pour les mouvement (diagonales en particulier) de divSquare*/


setInterval(() => {

    if (right) {
        divSquare.style.left = divSquare.offsetLeft + 35 + 'px';
    }
    if (left) {
        divSquare.style.left = divSquare.offsetLeft - 35 + 'px';
    }
    if (up) {
        divSquare.style.top = divSquare.offsetTop - 35 + 'px';
    }
    if (down) {
        divSquare.style.top = divSquare.offsetTop + 35 + 'px';
    }
    if (shoot) {

    }


}, 50);





/**Fonction qui crée des ennemies
 * 
 * 
 * @param {element} element 
 * @param {img} imageSrc 
 * @param {classname} nomClass 
 * @param {id} id 
 * @param {number} rn 
 * @param {number} taille 
 * @param {boolean} disapear 
 * @param {number} energie 
 */


function createEnnemiV2(element, imageSrc, nomClass, nomClass2, id, rn, taille, disapear = false, energie) {


    let ennemi2 = document.createElement(element);

    ennemi2.id = id + Math.floor(Math.random() * 500);

    ennemi2.src = imageSrc;

    ennemi2.style = "position:absolute";

    ennemi2.style.top = Math.floor(Math.random() * rn) + "px";

    ennemi2.classList.add(nomClass, nomClass2)

    ennemi2.style.width = taille + "px";

    playground.appendChild(ennemi2)

    ennemi2.setAttribute('data-health', energie);




    /*Fonction setInterval qui permet de gerer la colision de divSquare avec les ennemies*/


    let interval = setInterval(function () {

        if (ennemi2.offsetLeft < divSquare.offsetLeft + divSquare.offsetWidth &&
            ennemi2.offsetLeft + ennemi2.offsetWidth > divSquare.offsetLeft &&
            ennemi2.offsetTop < divSquare.offsetTop + divSquare.offsetHeight &&
            ennemi2.offsetHeight + ennemi2.offsetTop > divSquare.offsetTop) {
            // console.log("touché zbbrrrr");
            // ennemi2.remove()
            document.location.href = "gameOver.html"


            clearInterval(interval);
        }



    }, 100)

    /* Event qui permet de supprimer les ennemies prealablement crée une fois leurs animation terminer,
     *    
     * afin qu'ils ne surchage pas le DOM en données*/

    let ennemi2Interval = ennemi2.addEventListener("animationend", function () {

        if (disapear === false) {
            ennemi2.remove();
            clearInterval(ennemi2Interval);
        }


    })

}



/* Initialisation d'un compteur qui permetera de séquencer l'arrivé des ennemies dans le setInterval */

let count = 0;

/* SetInterval qui permet de generer des ennemies en boucle */

setInterval(function () {


    if (count < 10) {
        createEnnemiV2("img", "image/ennemi1Final.png", "ennemi", "ennemi", "bang", "1000", "150", false, 1)
        ennemiBullet()
    }
    if (count >= 10 && count < 15) {
        createEnnemiV2("img", "image/ennemi2.png", "ennemi2", "ennemi2", "bang", "1000", "400", false, 5)
    }
    if (count >= 15 && count < 25) {

        createEnnemiV2("img", "image/ennemi1Final.png", "ennemi", "ennemi", "bang", "1000", "150", false, 1)
        ennemiBullet()
        createEnnemiV2("img", "image/ennemi2.png", "ennemi2", "ennemi2", "bang", "1000", "400", false, 5)

    }
    if (count >= 25 && count < 45) {

        createEnnemiV2("img", "image/ennemi1Final.png", "ennemi", "ennemi", "bang", "1000", "150", false, 1)
        createEnnemiV2("img", "image/ennemi1Final.png", "ennemi", "ennemi", "bang", "1000", "150", false, 1)
        ennemiBullet()

    }


    count++;
}, 1000);


/*   Interval qui permet de poper le Boss au moment voulu   */


let interval = setInterval(function () {

    if (count === 50) {
        createEnnemiV2("img", "image/boss.png", "boss", "boss2", "bang", "1", "500", true, 50);
        clearInterval(interval);
    }
}, 1000)

setInterval(function () {

    bossBullet()
    // console.log(bossbullet());


}, 1000)




/* Creation des balles de mes ennemies*/

function ennemiBullet() {
    console.log("coucou");

    let enemies = document.querySelectorAll(".ennemi");
    let ennemi3 = enemies[Math.floor(Math.random() * enemies.length)];

    let playground = document.querySelector(".midground")

    let bullet = document.createElement('img');
    bullet.src = "image/shootSprite.png"

    bullet.classList.add('laserEnnemi');

    /*Placement des balles vis à vis de l'ennemi*/

    bullet.style.top = (ennemi3.offsetTop + ennemi3.offsetHeight - 45) + "px";
    bullet.style.left = (ennemi3.offsetLeft + ennemi3.offsetWidth - 260) + "px";

    playground.appendChild(bullet)



    /* Event qui permet de supprimer les ennemies prealablement crée une fois leurs animation terminer,
         *    
         * afin qu'ils ne surchage pas le DOM en données*/


    let bulletInterval = bullet.addEventListener("animationend", function () {

        bullet.remove();
        clearInterval(bulletInterval);
    })

    /* SetInterval qui permet d'appeller la fonction colision de mon player afin que les balles ennemies le touche grace au balles placer en argument de la fonction*/

    setInterval(function () {

        bulletColisionPlayer(bullet)
    }, 10)

}


/* Creation des balles du Boss*/

function bossBullet() {


    let enemies = document.querySelectorAll(".boss");
    let ennemi3 = enemies[Math.floor(Math.random() * enemies.length)];

    let playground = document.querySelector(".midground")

    let bullet = document.createElement('img');
    bullet.src = "image/fireBoss.png"
    console.log("coucou");
    bullet.classList.add('laserBoss');

    bullet.style.top = (ennemi3.offsetTop + ennemi3.offsetHeight - 260) + "px";
    bullet.style.left = (ennemi3.offsetLeft + ennemi3.offsetWidth - 450) + "px";

    let bullet2 = document.createElement('img');
    bullet2.src = "image/shootSprite.png"
    console.log("coucou");
    bullet2.classList.add('laserBoss');

    bullet2.style.top = (ennemi3.offsetTop + ennemi3.offsetHeight - 600) + "px";
    bullet2.style.left = (ennemi3.offsetLeft + ennemi3.offsetWidth - 400) + "px";


    playground.appendChild(bullet)
    playground.appendChild(bullet2)


    /* Event qui permet de supprimer les ennemies prealablement crée une fois leurs animation terminer,
         *    
         * afin qu'ils ne surchage pas le DOM en données*/


    let bulletInterval = bullet.addEventListener("animationend", function () {

        bullet.remove();
        bullet2.remove();
        clearInterval(bulletInterval);

    })


    /* SetInterval qui permet d'appeller la fonction colision des balle de mon player afin qu'elle le touche grace au balles placer en argument de la fonction*/


    setInterval(function () {


        bulletColisionPlayer(bullet)
        bulletColisionPlayer(bullet2)


    }, 10)


}



/* Creation des balles du Player*/


function bulletShootPlayer() {
    // let player = document.querySelector(".vaisseau");
    // let playground = document.querySelector(".midground")


    // let bullet = document.createElement('img');
    // bullet.src = "image/shootSprite.png"
    // bullet.classList.add('laser')

    // bullet.style.top = (player.offsetTop + player.offsetHeight - 50) + "px";
    // bullet.style.left = (player.offsetLeft + player.offsetWidth - 10) + "px";


    let player = document.querySelector(".vaisseau");
    let playground = document.querySelector(".midground")


    let bullet = document.createElement('img');
    bullet.src = "image/tireEncerclant.png"
    bullet.style.width = "170" + "px";
    bullet.classList.add('laser')

    bullet.style.top = (player.offsetTop + player.offsetHeight - 115) + "px";
    bullet.style.left = (player.offsetLeft + player.offsetWidth - 140) + "px";


    

    playground.appendChild(bullet);



    let bulletInterval = bullet.addEventListener("animationend", function () {

        bullet.remove();
        clearInterval(bulletInterval);
    })

    /* SetInterval qui permet d'appeller la fonction colision des ennemies de mon player afin que les ballesdu player les touches*/

    setInterval(function () {

        bulletColision(bullet)
        bulletColision2(bullet)
        bulletColisionBoss(bullet)

    }, 10);

    playAudioShootPlayer()

}

/*Colision ennemie2*/


function bulletColision2(bullet) {

    let ennemi = document.querySelectorAll(".ennemi2");

    for (let item of ennemi) {


        if (item.offsetLeft < bullet.offsetLeft + bullet.offsetWidth &&
            item.offsetLeft + item.offsetWidth > bullet.offsetLeft &&
            item.offsetTop < bullet.offsetTop + bullet.offsetHeight &&
            item.offsetHeight + item.offsetTop > bullet.offsetTop) {


            // item.src = "image/explosionEnnemi"

            /*Barre energie ennemi2*/


            item.setAttribute('data-health', item.getAttribute('data-health', 5) - 1);


            console.log(item.getAttribute('data-health'))

            // item.value -= 20



            if (item.getAttribute('data-health') == 0) {

                item.remove()
                playAudioExplosionEnnemi()
            }

            bullet.remove()





            // console.log("touché");


        }




    }

}


/*Colision ennemie2*/

function bulletColision(bullet) {

    let ennemi = document.querySelectorAll(".ennemi");

    for (let item of ennemi) {


        if (item.offsetLeft < bullet.offsetLeft + bullet.offsetWidth &&
            item.offsetLeft + item.offsetWidth > bullet.offsetLeft &&
            item.offsetTop < bullet.offsetTop + bullet.offsetHeight &&
            item.offsetHeight + item.offsetTop > bullet.offsetTop) {


            // item.src = "image/explosionEnnemi"

            item.setAttribute('data-health', item.getAttribute('data-health', 1) - 1);


            console.log(item.getAttribute('data-health'))

            // item.value -= 20



            if (item.getAttribute('data-health') == 0) {

                item.remove()
                playAudioExplosionEnnemi()
            }

            bullet.remove()





            // console.log("touché");


        }




    }

}

/*Colision Boss*/

function bulletColisionBoss(bullet) {

    let ennemi = document.querySelectorAll(".boss");

    for (let item of ennemi) {


        if (item.offsetLeft < bullet.offsetLeft + bullet.offsetWidth &&
            item.offsetLeft + item.offsetWidth > bullet.offsetLeft &&
            item.offsetTop < bullet.offsetTop + bullet.offsetHeight &&
            item.offsetHeight + item.offsetTop > bullet.offsetTop) {


            // item.src = "image/explosionEnnemi"

            item.setAttribute('data-health', item.getAttribute('data-health', 100) - 1);


            console.log(item.getAttribute('data-health'))

            // item.value -= 20



            if (item.getAttribute('data-health') == 0) {

                item.remove()
                playAudioExplosionEnnemi()

                document.location.href = "youWin.html"
            }

            bullet.remove()

            setInterval(function () {

                bulletColisionPlayer(bullet)
            }, 10)





            // console.log("touché");


        }




    }

}

/*Colision Player au balles*/

function bulletColisionPlayer(bullet) {

    let item = document.querySelector(".vaisseau");


    if (item.offsetLeft < bullet.offsetLeft + bullet.offsetWidth &&
        item.offsetLeft + item.offsetWidth > bullet.offsetLeft &&
        item.offsetTop < bullet.offsetTop + bullet.offsetHeight &&
        item.offsetHeight + item.offsetTop > bullet.offsetTop) {

        document.location.href = "gameOver.html"


        // item.src = "image/explosionEnnemi"

        // bullet.remove()

        // playAudioExplosionEnnemi()

        // console.log("touché");


    }

}

/*Bruit de tir du Player*/

function playAudioShootPlayer() {

    var sound = document.createElement('audio');
    sound.id = 'audio-player';
    // sound.controls = 'controls';
    sound.src = 'sound/shootSound.mp3';
    sound.type = 'audio/mpeg';

    // document.getElementById('song').appendChild(sound);


    sound.play();
}

function pauseAudio() {
    myAudio.pause();
}


/*Bruit explosion ennemie*/

function playAudioExplosionEnnemi() {

    var explosion = document.createElement('audio');
    explosion.id = 'audio-player';
    // Explosion.controls = 'controls';
    explosion.src = 'sound/explosionEnnemi.mp3';
    explosion.type = 'audio/mpeg';

    // document.getElementById('song').appendChild(Explosion);


    explosion.play();
}

function pauseAudio() {
    myAudio.pause();
}


/*fonction pour pouvoir jouer la musique*/


function playMusic() {

    var music = document.createElement('audio');
    music.id = 'audio-player';
    // Explosion.controls = 'controls';
    music.src = 'sound/musicGame.mp3';
    music.type = 'audio/mpeg';

    // document.getElementById('song').appendChild(Explosion);


    music.play();
}

function pauseAudio() {
    myAudio.pause();
}



/*Fonction pour image explosion(pas encore integrer dans le code)*/

function explosionEnnemi() {

    let explosionEnnemi = document.createElement("img");
    explosionEnnemi.src = "image/explosionEnnemi.gif"

}



/*Nouvelle arme pour le player*/

function newAmo() {
    let player = document.querySelector(".vaisseau");
    let playground = document.querySelector(".midground")


    let bullet = document.createElement('img');
    bullet.src = "image/tireEncerclant.png"
    bullet.style.width = "170" + "px";
    bullet.classList.add('laser')

    bullet.style.top = (player.offsetTop + player.offsetHeight - 115) + "px";
    bullet.style.left = (player.offsetLeft + player.offsetWidth - 140) + "px";

}
